# Chat4XAI

Supplementary Material for "An AI Chatbot for Explaining Deep Reinforcement Learning Decisions of Service-oriented Systems"

### Proof-of-Concept Realization of Chat4XAI 
- [Jupyter Notebook](notebook/experiment_execution.ipynb)

### Results of comparative study (Chat4XAI vs human participants)
- [XLSX](results/comparative-study.xlsx)

### Replication package for XRL-DINE (underlying explanation technique) 
- [XRL-DINE](https://git.uni-due.de/rl4sas/xrl-dine)

### Detailed results of XRL-DINE user study
- [User study](https://git.uni-due.de/rl4sas/xrl-dine/-/blob/main/user-study.xlsx)

### DINEs (decomposed interestingness elements) of the underlying exlanation technique XRL-DINE for the considered experiment scenario
- [JSON](logs/RL-trace-excerpt.json)

### Background material on ChatGPT
 - [OpenAI ChatGPT API](https://platform.openai.com/docs/api-reference)
 - [Hyperparameters to control text generation](https://community.openai.com/t/cheat-sheet-mastering-temperature-and-top-p-in-chatgpt-api-a-few-tips-and-tricks-on-controlling-the-creativity-deterministic-output-of-prompt-responses/172683)
